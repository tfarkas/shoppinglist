
export class User {
    id?: string;
    uid: string;
    username?: string;
    gender?: string;
    profilepicture?: string;
    email: string;

    constructor(param?: User) {
        Object.assign(this, param);
    }

} 
