import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument,
  AngularFirestoreCollection
} from 'angularfire2/firestore';
import firebase from 'firebase/app';

import { fromPromise } from 'rxjs/internal/observable/fromPromise';
import { tap, map, filter, first } from 'rxjs/internal/operators';
import { Observable } from 'rxjs/Rx';
import { User } from '../../model/user';
import { from } from 'rxjs/internal/observable/from';
import { combineLatest } from 'rxjs/internal/observable/combineLatest';
import { of } from 'rxjs/internal/observable/of';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map'

@Injectable()
export class AuthProvider {


  userRef: AngularFirestoreCollection<User>;
  users$: Observable<User[]>;

  constructor(public afAuth: AngularFireAuth, private afs: AngularFirestore) {

    this.userRef = this.afs.collection<User>('user');
    // this.categories$ = this.categoriesRef.valueChanges();

    this.users$ = this.userRef.snapshotChanges()
      .pipe(
        map(actions => {
          return actions.map(action => {
            const data = action.payload.doc.data() as User;
            data.id = action.payload.doc.id;
            const id = action.payload.doc.id;
            return { id, ...data };
          });
        })
      );
  }

  loginUser(newEmail: string, newPassword: string): Observable<any> {
    return fromPromise(this.afAuth.auth.signInWithEmailAndPassword(newEmail, newPassword));
  }

  resetPassword(email: string): Observable<void> {
    return fromPromise(this.afAuth.auth.sendPasswordResetEmail(email));
  }

  signupUser(newEmail: string, newPassword: string): Observable<firebase.auth.UserCredential> {
    return fromPromise(
      this.afAuth.auth.createUserWithEmailAndPassword(newEmail, newPassword))
      .pipe(
        tap((data: firebase.auth.UserCredential) => {
          this.add({ uid: data.user.uid, email: data.user.email, username: data.user.email, profilepicture: '' }, data.user.uid);
        })
      );
  }

  logOut(): Observable<any> {
    return fromPromise(this.afAuth.auth.signOut());
  }

  getUsers(): Observable<User[]> {
    return this.users$;
  }

  userItem: Observable<User[]>;
  actUserCollection;
  getUser(id: string): Observable<User[]> {

    this.actUserCollection = this.afs.collection<User>('user', ref => {
      return ref
        .where('uid', '==', id)
    });
    this.userItem = this.actUserCollection.snapshotChanges().pipe(
      map(actions => {
        return actions.map(action => {
          const data = action.payload.doc.data() as User;
          data.id = action.payload.doc.id;
          const id = action.payload.doc.id;
          return { id, ...data };
        });
      })
    );

    return this.userItem;
  }

  get timestamp() {
    return firebase.firestore.FieldValue.serverTimestamp()
  }

  add<User>(data, uid) {
    const timestamp = this.timestamp
    return this.userRef.add({ 
      ...data, 
      updatedAt: timestamp,
      createdAt: timestamp 
    });
  }

  update<User>(data: any, id: string) {
    return this.afs.doc(`user/${id}`).update({
      ...data,
      updatedAt: this.timestamp
    })
  }

}
