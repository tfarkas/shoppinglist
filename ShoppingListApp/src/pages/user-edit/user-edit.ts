import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth';
import { User } from '../../model/user';

/**
 * Generated class for the UserEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-edit',
  templateUrl: 'user-edit.html',
})
export class UserEditPage implements OnInit {

  uid: string;
  username: string;
  gender: string;
  authenticatedUser: User;

  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AuthProvider) {
  }

  ngOnInit(): void {
    this.uid = this.navParams.get('uid');
    this.auth.getUser(this.uid).subscribe(data => {
      this.authenticatedUser = data[0];
      this.gender = data[0].gender;
      this.username = data[0].username;
    });
  }

  updateData() {
    this.auth.update({gender: this.gender, username: this.username}, this.authenticatedUser.id);
    this.navCtrl.pop()
  }
}
