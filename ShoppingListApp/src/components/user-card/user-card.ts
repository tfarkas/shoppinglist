import { Component, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../../model/user';

@Component({
  selector: 'user-card',
  templateUrl: 'user-card.html'
})
export class UserCardComponent {

  @Input() user: User;
  @Output() editUser: EventEmitter<any> = new EventEmitter();
  edit() {        
    this.editUser.emit(this.user.uid);
  }
}
