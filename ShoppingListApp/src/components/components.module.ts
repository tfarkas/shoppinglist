import { NgModule } from '@angular/core';
import { UserCardComponent } from './user-card/user-card';
@NgModule({
	declarations: [UserCardComponent],
	imports: [],
	exports: [UserCardComponent]
})
export class ComponentsModule {}
