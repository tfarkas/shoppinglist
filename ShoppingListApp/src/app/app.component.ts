import { Component, ViewChild, OnInit, AfterViewInit } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';

import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../model/user';
import { AuthProvider } from '../providers/auth/auth';

@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{ title: string, component: any }>;
  authenticatedUser: User;
  authenticatedUserId: string;

  constructor(public platform: Platform, private auth: AuthProvider, private afAuth: AngularFireAuth, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();
  }

  ngOnInit(): void {
    const authObserver = this.afAuth.authState.subscribe(user => {
      if (user) {

        this.rootPage = HomePage;

        this.authenticatedUserId = user.uid;
        this.auth.getUser(this.authenticatedUserId).subscribe(data => {
          this.authenticatedUser = data[0];
        });
      } else {
        this.rootPage = 'LoginPage';
        authObserver.unsubscribe();
      }
    });

    this.pages = [
      { title: 'Aktuális', component: HomePage },
      // { title: 'List', component: ListPage }
    ];

    // this.auth.getUsers().subscribe(console.log);
  }
  ionViewDidLoad() {

  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }

  editClicked(ev) {
    // GO TO EDITUSER PAGE WITH PARAM EV - UID
    this.nav.push('UserEditPage', { uid: ev });
  }
}
