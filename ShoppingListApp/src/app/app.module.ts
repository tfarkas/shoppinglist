import { SignupPage } from './../pages/signup/signup';
import { LoginPage } from './../pages/login/login';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthProvider } from '../providers/auth/auth';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { UserCardComponent } from '../components/user-card/user-card';
import { ResetPasswordPage } from '../pages/reset-password/reset-password';
import { LoginPageModule } from '../pages/login/login.module';
import { HomePageModule } from '../pages/home/home.module';
import { AngularFirestoreModule, AngularFirestore } from 'angularfire2/firestore';
import { AngularFireDatabaseModule, } from 'angularfire2/database';

const firebaseConfig = {
  apiKey: 'AIzaSyCB84DZCT7QIjaLZkIb7SuDKRggM3apzAk',
  authDomain: 'shoppinglist-48071.firebaseapp.com',
  databaseURL: 'https://shoppinglist-48071.firebaseio.com',
  projectId: 'shoppinglist-48071',
  storageBucket: 'shoppinglist-48071.appspot.com',
  messagingSenderId: '854043174982'
};


@NgModule({
  declarations: [
    MyApp,
    ListPage,
    UserCardComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFirestoreModule.enablePersistence(),
    LoginPageModule,
    HomePageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ListPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider
  ]
})
export class AppModule {
  // constructor(private afs: AngularFirestore ) {
  //   afs.firestore.settings({timestampsInSnapshots: true});
  // }
}
